SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `permission`, `created_at`, `updated_at`) VALUES
(1,	0,	1,	'Dashboard',	'fa-bar-chart',	'/',	NULL,	NULL,	NULL),
(2,	0,	2,	'Admin',	'fa-tasks',	'',	NULL,	NULL,	NULL),
(3,	2,	3,	'Users',	'fa-users',	'auth/users',	NULL,	NULL,	NULL),
(4,	2,	4,	'Roles',	'fa-user',	'auth/roles',	NULL,	NULL,	NULL),
(5,	2,	5,	'Permission',	'fa-ban',	'auth/permissions',	NULL,	NULL,	NULL),
(6,	2,	6,	'Menu',	'fa-bars',	'auth/menu',	NULL,	NULL,	NULL),
(7,	2,	7,	'Operation log',	'fa-history',	'auth/logs',	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `admin_operation_log`;
CREATE TABLE `admin_operation_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1,	1,	'admin',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 21:40:57',	'2022-03-15 21:40:57'),
(2,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 21:49:31',	'2022-03-15 21:49:31'),
(3,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 21:52:50',	'2022-03-15 21:52:50'),
(4,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 21:52:57',	'2022-03-15 21:52:57'),
(5,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 21:53:17',	'2022-03-15 21:53:17'),
(6,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 21:53:30',	'2022-03-15 21:53:30'),
(7,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 21:53:36',	'2022-03-15 21:53:36'),
(8,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 21:53:51',	'2022-03-15 21:53:51'),
(9,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 21:54:08',	'2022-03-15 21:54:08'),
(10,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 21:54:54',	'2022-03-15 21:54:54'),
(11,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 21:55:09',	'2022-03-15 21:55:09'),
(12,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 21:57:27',	'2022-03-15 21:57:27'),
(13,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"2\"}',	'2022-03-15 21:59:14',	'2022-03-15 21:59:14'),
(14,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"2\"}',	'2022-03-15 21:59:32',	'2022-03-15 21:59:32'),
(15,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"2\"}',	'2022-03-15 22:00:15',	'2022-03-15 22:00:15'),
(16,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"2\"}',	'2022-03-15 22:00:42',	'2022-03-15 22:00:42'),
(17,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"2\"}',	'2022-03-15 22:01:04',	'2022-03-15 22:01:04'),
(18,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"2\"}',	'2022-03-15 22:01:16',	'2022-03-15 22:01:16'),
(19,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"2\"}',	'2022-03-15 22:01:22',	'2022-03-15 22:01:22'),
(20,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'{\"_pjax\":\"#pjax-container\"}',	'2022-03-15 22:01:25',	'2022-03-15 22:01:25'),
(21,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\",\"_pjax\":\"#pjax-container\"}',	'2022-03-15 22:01:27',	'2022-03-15 22:01:27'),
(22,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\"}',	'2022-03-15 22:02:12',	'2022-03-15 22:02:12'),
(23,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\"}',	'2022-03-15 22:02:31',	'2022-03-15 22:02:31'),
(24,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\"}',	'2022-03-15 22:03:02',	'2022-03-15 22:03:02'),
(25,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\"}',	'2022-03-15 22:03:11',	'2022-03-15 22:03:11'),
(26,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\"}',	'2022-03-15 22:04:53',	'2022-03-15 22:04:53'),
(27,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\"}',	'2022-03-15 22:05:32',	'2022-03-15 22:05:32'),
(28,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\"}',	'2022-03-15 22:05:54',	'2022-03-15 22:05:54'),
(29,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\"}',	'2022-03-15 22:05:59',	'2022-03-15 22:05:59'),
(30,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\"}',	'2022-03-15 22:06:22',	'2022-03-15 22:06:22'),
(31,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\"}',	'2022-03-15 22:06:37',	'2022-03-15 22:06:37'),
(32,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'{\"_pjax\":\"#pjax-container\"}',	'2022-03-15 22:06:42',	'2022-03-15 22:06:42'),
(33,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\",\"_pjax\":\"#pjax-container\"}',	'2022-03-15 22:06:45',	'2022-03-15 22:06:45'),
(34,	1,	'admin/jobs/job',	'GET',	'127.0.0.1',	'{\"id\":\"1\"}',	'2022-03-15 22:07:28',	'2022-03-15 22:07:28'),
(35,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'{\"_pjax\":\"#pjax-container\"}',	'2022-03-15 22:07:30',	'2022-03-15 22:07:30'),
(36,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 22:07:39',	'2022-03-15 22:07:39'),
(37,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 22:07:49',	'2022-03-15 22:07:49'),
(38,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 22:07:53',	'2022-03-15 22:07:53'),
(39,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 22:09:03',	'2022-03-15 22:09:03'),
(40,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 22:09:19',	'2022-03-15 22:09:19'),
(41,	1,	'admin/jobs',	'GET',	'127.0.0.1',	'[]',	'2022-03-15 22:09:28',	'2022-03-15 22:09:28'),
(42,	1,	'admin/jobs/job/new',	'GET',	'127.0.0.1',	'{\"_pjax\":\"#pjax-container\"}',	'2022-03-15 22:09:29',	'2022-03-15 22:09:29');

DROP TABLE IF EXISTS `admin_permissions`;
CREATE TABLE `admin_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`),
  UNIQUE KEY `admin_permissions_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1,	'All permission',	'*',	'',	'*',	NULL,	NULL),
(2,	'Dashboard',	'dashboard',	'GET',	'/',	NULL,	NULL),
(3,	'Login',	'auth.login',	'',	'/auth/login\r\n/auth/logout',	NULL,	NULL),
(4,	'User setting',	'auth.setting',	'GET,PUT',	'/auth/setting',	NULL,	NULL),
(5,	'Auth management',	'auth.management',	'',	'/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs',	NULL,	NULL);

DROP TABLE IF EXISTS `admin_role_menu`;
CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1,	2,	NULL,	NULL);

DROP TABLE IF EXISTS `admin_role_permissions`;
CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1,	1,	NULL,	NULL);

DROP TABLE IF EXISTS `admin_role_users`;
CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1,	1,	NULL,	NULL);

DROP TABLE IF EXISTS `admin_roles`;
CREATE TABLE `admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`),
  UNIQUE KEY `admin_roles_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1,	'Administrator',	'administrator',	'2022-03-15 19:00:16',	'2022-03-15 19:00:16');

DROP TABLE IF EXISTS `admin_user_permissions`;
CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE `admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `admin_users` (`id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'admin',	'$2y$10$v1qigXOM0gtgHhtxN6fAfuvqfhhhA9sUK.BqV2S/VaH7jI9y9cn.a',	'Administrator',	NULL,	'wFJKzuRShRnNp2ahEUs0g90GwA2MjDkExVE3TFxndRRpaprdwYt3CFyu79Mi',	'2022-03-15 19:00:16',	'2022-03-15 19:00:16');

DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `job_applications`;
CREATE TABLE `job_applications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `job_applications` (`id`, `user_id`, `job_id`, `created_at`, `updated_at`) VALUES
(1,	1,	2,	NULL,	NULL),
(3,	1,	2,	NULL,	NULL);

DROP TABLE IF EXISTS `job_categories`;
CREATE TABLE `job_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `closing_date` timestamp NOT NULL,
  `address1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `job_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_title_created_at_closing_date_index` (`title`,`created_at`,`closing_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `jobs` (`id`, `title`, `description`, `closing_date`, `address1`, `address2`, `city`, `postcode`, `country`, `company_id`, `job_category_id`, `created_at`, `updated_at`) VALUES
(1,	'Senior Web Developer',	'We are looking for a senior web developer ',	'2022-03-30 23:00:00',	'',	'',	'London',	'',	'',	1,	1,	'2022-03-15 00:00:00',	NULL),
(2,	'Junior PHP Developer',	'We are looking for a junior PHP developer ',	'2022-03-30 23:00:00',	'',	'',	'Manchester',	'',	'',	1,	1,	'2022-03-15 00:00:00',	NULL);

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(3,	'2019_08_19_000000_create_failed_jobs_table',	1),
(4,	'2019_12_14_000001_create_personal_access_tokens_table',	1),
(5,	'2022_03_15_110737_create_companies_table',	1),
(6,	'2022_03_15_110755_create_jobs_table',	1),
(7,	'2022_03_15_110806_create_job_applications_table',	1),
(8,	'2022_03_15_111149_create_job_categories_table',	1),
(9,	'2016_01_04_173148_create_admin_tables',	2);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'Firas',	'fa@firas.org',	NULL,	'$2y$10$dZ2jU94ULb/wWzF0Ln8e6e9ddb6Ev2ur6Ldy2Emu7J2W5L7u8Gv86',	NULL,	'2022-03-15 11:26:35',	'2022-03-15 11:26:35');

-- 2022-03-15 22:22:55
