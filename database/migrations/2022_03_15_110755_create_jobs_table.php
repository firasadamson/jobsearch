<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->timestamp('closing_date');
            $table->string('address1');
            $table->string('address2');
            $table->string('city');
            $table->string('postcode');
            $table->string('country');
            $table->integer('company_id');
            $table->integer('job_category_id');
            $table->timestamps();

            $table->index([
                'title', 
                'created_at', 
                'closing_date'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
