# JobSearch
A job search website built using Laravel. 

## The Exercise
* To create a job advertising website.
* Jobs should be searchable by keyword and/or filtered by date of creation or closing date
* Applications will likely be stored in a database, potentially with associated CV documentation if required. 
* The website should have an administration system allow an admin to perform basic  CRUD operations on jobs. The administrator may also require some view of the applications and any associated statistics.

## Files
* Laravel files
* MySQL database - jobasearch-db.sql (found in the root of the project)
* Database Schema - db-schema.png (found in the root of the project)

## What was achieved
* The homepage shows the list of jobs available + a search box to search by keyword and/or filter  by creation or closing date in ascending and descending order.
* Login, registration and forgot password pages. I used 'composer require laravel/breeze --dev' to set up the auth.
* The job listing page with an apply button which only shows if users are logged in.
* I created the admin section using https://laravel-admin.org/docs/en/README to speed up the development. I've never used this before but managed to get it set up but didn't get a chance to complete everything.

## Frontend Links
* http://127.0.0.1:8000
* http://127.0.0.1:8000/job/1 (1 being the job_id) 
* http://127.0.0.1:8000/login
* http://127.0.0.1:8000/register 
* http://127.0.0.1:8000/forgot-password 

## Admin Links
* http://127.0.0.1:8000/admin/auth/login (user: admin, password: admin)

### I didn't manage to add the Jobs section in the menu so here are the direct links
  * http://127.0.0.1:8000/admin/jobs
  * http://127.0.0.1:8000/admin/jobs/job/?id=1 (edit view)
  * http://127.0.0.1:8000/admin/jobs/new (nothing on this page as I ran out of time)

## What wasn't achieved
### There are a number things that I didn't manage to complete in the time given to me.
* it is mostly in the admin area that I did not complete. I managed to list all jobs in a list. I also managed to create a page that would show the job in edit view i.e. a form with update and delete buttons. However, The only thing that shows is the job title in an input field and the buttons that don't do anything. 
* In terms of the frontend, there is always room for improvement. I created several tables with fields to hold data that will make the job listings more detailed. But for this exercise I only managed to use a few of the fields. 
* I did not manage to set up an area for users to upload their CV which is then used when applying for jobs.
* Finally, I didn't spend time styling as the focus was on the functionalities.

## What I could do to improve
### Given that I only had 4 hours I tried to achieve as much as possible within the timeframe. 
* Something that I wanted to do was once someone has applied, it would change the apply button's state to disabled and show as "Applied". 
* Also show in the User's dashboard a list of jobs they have applied for.
* In the admin section, show a list of all job applications, in different ways; by users -> applications, by job -> applications, and a list of all applications in date order of submissions with filters/search options. Create reports.

## Time Logs
So I started working on this on Thursday 10 March and then 27 minutes in I encounted an issue when trying to run the 'php artisan' command. After doing some research it turned out my PHP was not version 8 or above. I attempted to upgrade but it kept failing, so in the end I decided to just set up another local server where PHP v8 was available, which solved the problem. I went away on Friday and came back late Monday, hence the development continued today. I used my timer on my phone to track and log the development time. Please find a breakdown below.

### THU
* 21:46 to 22:13  (27 mins)

### TUE
* 11:05 to 11:26 (21 mins)
* 11:42 to 12:27 (45 mins)
* 17:15 to 17:50 (35 mins)
* 18:56 to 19:31 (35 mins)
* 19:47 to 20:32 (45 mins)
* 21:37 to 22:09 (32 mins)

**TOTAL: 240 mins**

If you have any questions, please do not hesitate to get in toucn.