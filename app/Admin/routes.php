<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->get('/jobs', 'JobController@index')->name('jobs');
    $router->get('/jobs/job', 'JobController@job')->name('job');
    $router->get('/jobs/job/new', 'JobController@new')->name('new');

});
