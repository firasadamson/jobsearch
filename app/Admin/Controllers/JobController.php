<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Job;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Form;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Illuminate\Support\Facades\DB;


class JobController extends Controller
{
    public function index(Content $content)
    {
        return $content
        ->title('Jobs')
        ->row(function (Row $row) {
            
            $row->column(4, function (Column $column) {
                $column->append('<p style="margin-bottom:45px;"><a href="/admin/jobs/job/new">New Job</a></p>'); 
                $jobs = DB::table('jobs')->get();
                foreach( $jobs as $job ) {
                    $column->append('<p style="border-bottom:1px solid #ddd; padding-bottom:10px;"><a href="/admin/jobs/job/?id='.$job->id.'">' . $job->title . '</a></p>'); 
                };
            });
        });
    }

    public function job(Content $content)
    {
        global $job;
        $job = DB::table('jobs')->where('id','=', $_GET['id'])->first();

        return $content
        ->title( $job->title )
        ->description('<a href="/admin/jobs">Back</a>')
        ->row(function (Row $row) {
            
            $row->column(4, function (Column $column) {
                global $job;
                $column->append('<input value="'.$job->title.'" />');
                $column->append('<div style="margin-top:25px"><input type="submit" value="Update" />');
                $column->append(' <input type="submit" value="Delete" /></div>');
                // $column->append('<p style="border-bottom:1px solid #ddd; padding-bottom:10px;"><a href="/admin/jobs/job/'.$job->id.'">' .  . '</a></p>'); 
            });
        });
    }

    public function new(Content $content)
    {
        return $content
        ->title( 'New Job' )
        ->description('<a href="/admin/jobs">Back</a>');
    }
}
