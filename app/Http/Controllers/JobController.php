<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobController extends Controller
{
    public function get_all(Request $request)
    {
        $search = $request->query('search') ?? '';
        $filter = $request->query('filter') == 'creation' ? 'created_at' : 'closing_date';
        $orderby = $request->query('orderby') ?? 'desc';

        // search=Senior&filter=creation&orderby=desc

        $jobs = DB::table('jobs')
        ->where('title','like', '%'.$search.'%')
        ->orderby($filter, $orderby)
        ->get();




        return view('home', [
            'jobs' => $jobs, 
            'search' => $search,
            'filter' => $filter,
            'orderby' => $orderby
        ]);


    }


    public function get($id)
    {
        $job = DB::table('jobs')->where('id','=',$id)->first();
        return view('job', ['job' => $job]);
    }

}
