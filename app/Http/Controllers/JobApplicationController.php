<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class JobApplicationController extends Controller
{
    //

    public function apply($id)
    {

        DB::table('job_applications')->upsert(
            ['user_id' => Auth::id(), 'job_id' => $id],
            ['user_id', 'job_id'], ['job_id']
        );

        return redirect('/job/' . $id . '/?success=1');
    }

}
