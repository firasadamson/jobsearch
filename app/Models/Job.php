<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    use HasFactory;

    // protected $dateFormat = 'd/m/Y';

    // protected $table = 'jobs';
    // protected $primaryKey = 'id';
    protected $casts = [
        'closing_date'  => 'date:Y-m-d',
        'created_at' => 'date:Y-m-d',
    ];

}
