@include('_head')         
    
<div>
    @if ( isset($_GET['success']) )
    <div class="mb-4" style="font-weight:bold;border:1px solid #ccc; padding:25px;">You have successfully applied for this job</div>
    @endif

    <h1>{{ $job->title }}</h1>
    <div class="mb-4"><small>Posted on: {{ \Carbon\Carbon::parse($job->created_at)->format('d M Y') }} &bull; Location: <strong>{{ $job->city }}</strong></small></div>

    <div class="mb-4">{{ $job->description }}</div>
    
    <div class="mb-4">Closing date: {{ \Carbon\Carbon::parse($job->closing_date)->format('d M Y') }}</div>

    @if (Route::has('login'))
        @auth
            <div class="mt-6"><a class="btn" href="/job/{{ $job->id }}/apply">Apply</a></div>
        @endauth
    @endif
</div>

@include('_foot')