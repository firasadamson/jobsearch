@include('_head')         
    
<div class="mb-10">
    <h1>Welcome to JobSearch</h1>
    <form action="/" method="GET" class="search-box">
        
        <label>
            <span class="absolute inset-y-0 left-0 flex items-center pl-2">
                <svg class="h-5 w-5 fill-slate-300" viewBox="0 0 20 20"><!-- ... --></svg>
            </span>
            <input value="{{ $search }}" placeholder="Find your dream job..." type="text" name="search" />
            <select name="filter">
                <option {{ $filter == 'creation' ? 'selected' : '' }} value="creation">Creation Date</option>
                <option {{ $filter == 'closing' ? 'selected' : '' }} value="closing">Closing Date</option>
            </select>

            <select name="orderby">
                <option {{ $orderby == 'desc' ? 'selected' : '' }} value="desc">Descending</option>
                <option {{ $orderby == 'asc' ? 'selected' : '' }} value="asc">Ascending</option>
            </select>

            <input type="submit" class="btn" value="Search" />

        </label>

    </form>
</div>


@foreach ($jobs as $job)
    <a class="job-list-item" href="/job/{{ $job->id }}">
        <div class="job-title">{{ $job->title }}, {{ $job->city }}</div>
        <div>{{ $job->description }}</div>
        <div><small>Posted on: <strong>{{ \Carbon\Carbon::parse($job->created_at)->format('d M Y') }}</strong> &bull; Closing date: <strong>{{ \Carbon\Carbon::parse($job->closing_date)->format('d M Y') }}</strong></small></div>
    </a>
@endforeach

@include('_foot')